import networkx as nx
import itertools 
from Tree import *
import numpy as np

def print_dendro(the_array,Y,name):
    print("   DENDRO: printing a matrix shaped: ", the_array.shape)
    # Printing dendrogram options
    # this has to be of the size of the nb of classes

    rgb_list = [ #red, green,     blue, yellow,      pink, turkquise,      black, brown
        "FF0000", "00FF00", "0000FF", "FFFF00", "FF00FF", "00FFFF", "000000", 
        "800000", "008000", "000080", "808000", "800080", "008080", "808080", 
        "C00000", "00C000", "0000C0", "C0C000", "C000C0", "00C0C0", "C0C0C0", 
        "400000", "004000", "000040", "404000", "400040", "004040", "404040", 
        "200000", "002000", "000020", "202000", "200020", "002020", "202020", 
        "600000", "006000", "000060", "606000", "600060", "006060", "606060", 
        "A00000", "00A000", "0000A0", "A0A000", "A000A0", "00A0A0", "A0A0A0", 
        "E00000", "00E000", "0000E0", "E0E000", "E000E0", "00E0E0", "E0E0E0", 
    ]
    the_length = the_array.shape[0]+1

    plt.clf()
    
    rgb = {i : "#"+rgb_list[i] for i in range(len(rgb_list))}
    
    hierarchy.dendrogram(the_array[:,:] ,color_threshold=0.00001,labels=[i for i in range(the_length)])

    label_colors = {i: rgb[Y[i]] for i in range(the_length)}

    ax = plt.gca()
    xlbls = ax.get_xmajorticklabels()

    for lbl in xlbls:

        if len(lbl.get_text()) > 0:
            lbl.set_color(label_colors[int(lbl.get_text())])

    plt.savefig(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')+name+'_plot.png')

def GetGTTree(bigleavessizes, proba, labels):
    from scipy.cluster.hierarchy import linkage
    from copy import deepcopy
    n = sum(bigleavessizes)
    distancematrix = []
    for u in range(n):
        proba_u = deepcopy(proba[labels[u]])
        all_proba_u = [] 
        for j in range(len(proba_u)):
            all_proba_u += [1-proba_u[j]]*bigleavessizes[j]
        all_proba_u[u] = 0
        distancematrix.append(deepcopy(all_proba_u))

    dendro = linkage(distancematrix, method='single')
    return dendro    
    
def GenerateGraph(proba, bigleavessizes):

    n = sum(bigleavessizes)
    G = nx.Graph()
    labels = []
    i = 0
    for lab in range(len(bigleavessizes)):
        for u in range(bigleavessizes[lab]):
            labels.append(lab)
            i+=1
    count = 0

    permuted = np.random.permutation(n)
    cpylabels = labels[:]
    labels = [cpylabels[permuted[x]-1] for x in range(n)  ]


    for u,v in list(itertools.combinations_with_replacement(range(n), r=2)):
        clustu = labels[u]
        clustv = labels[v]
        coin = np.random.random() 
        if coin < proba[clustu][clustv]:
            count = count + 1
            G.add_edge(u,v, weight=1.0)

    print("Number of edges:", count)      
    return G, labels

def generateTree(layers, prob,  bonus):
    if layers == 0:
        return Node(0, min(prob,1),0)
    newNode = Node(0, min(prob,1))
    left = generateTree( layers-1, prob+bonus,bonus)
    newNode.left = left
    right= generateTree( layers-1, prob+bonus,bonus)
    newNode.right = right
    return newNode

def binarylimits(n, k):
    import math
    from copy import deepcopy

 

    bigleavessizes = []
    for i in range(k-1):
        bigleavessizes.append(int(n/k))
    bigleavessizes.append(n-sum(bigleavessizes))    

    print (sum(bigleavessizes), len(bigleavessizes))

    logk = int(math.log(k,2))
    pmin = 0
    layers = logk+1
    pmax = 0.8
    bonus = 1.0*(pmax-pmin)/layers

    root = generateTree(layers,pmin,bonus)

    root.setphi(0)
    #------------- GENERATE Prob Matrix

    thatlist = root.generateListofLeaves()[:k]

    pairs = list(itertools.combinations_with_replacement(thatlist, 2))

    Matrix = [[0. for i in range(k)] for elem in range(k)]
    for elem in sorted(pairs):
        LCA = findLCAwithoutlabels(root,elem[0],elem[1])
        Matrix[elem[0].phi][elem[1].phi] = LCA.prob
        Matrix[elem[1].phi][elem[0].phi] = LCA.prob

 

 

    print("Probability Matrix:" )
 
    print(np.matrix(Matrix))

    G, labels = GenerateGraph(Matrix, bigleavessizes)
    dendro = GetGTTree(bigleavessizes, Matrix, labels)
    return G, labels, dendro

def binarylimitsspecial(n, k):
    import math
    from copy import deepcopy

    print("PARAMS", n, k    )

   
    
    
    r=0.39
    q=0.49
    p1 = 1
    p2 = 0.49
    p3= 0.62
    Matrix = np.array([[p1,q,r],[q,p2,r],[r,r,p3]]) 

    bigleavessizes =[int(n/3),int(n/3)]
    bigleavessizes.append(n-sum(bigleavessizes))  


    print("Probability Matrix:" )
    # if k < 20:
    print(np.matrix(Matrix))

    G, labels = GenerateGraph(Matrix, bigleavessizes)
    dendro = GetGTTree(bigleavessizes, Matrix, labels)
    return G, labels, dendro



