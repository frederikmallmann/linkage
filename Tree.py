# Python program to find LCA of n1 and n2 using one
# traversal of Binary tree
 
# A binary tree node
# This code is from https://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-search-tree/ 
# contributed by Nikhil Kumar Singh(nickzuck_007)
class Node:
    prob = 0 
    # Constructor to create a new tree node
    def __init__(self, key, prob, number=0):
        self.key = key 
        self.left = None
        self.right = None
        self.prob = prob
        self.number = number




    def add(self, node):
        if self.left == None:
            self.left = node
        else:
            self.right = node 
               
    def treePropertyCheck(self):
        #NOTFINISHED
        if self.left == None or self.right == None:
            return 42
        else:
            return self.left.getNumber() + self.right.getNumber()     

    def getNumber(self):
  
        if self.left == None or self.right == None:
            return self.number
        else:
            return self.left.getNumber() + self.right.getNumber()     


    def setphi(self,value):
        if self.left == None:
            self.phi= value
            return value + 1
        else:
            theirval = self.left.setphi(value)
            return self.right.setphi(theirval)     
            
    

    def generateList(self):
        if self.left == None or self.right == None:
            return [self.number]
        
        lst = self.left.generateList()
        lst.extend(self.right.generateList())
        return lst          

    def generateListofLeaves(self):
        if self.left == None or self.right == None:
            return [self]
        lst = self.left.generateListofLeaves()
        lst.extend(self.right.generateListofLeaves())
        return lst  

# This function returns pointer to LCA of two given
# values n1 and n2
# This function assumes that n1 and n2 are present in
# Binary Tree

def findLCAwithoutlabels(root, n1, n2):
    #print "INPUT ", root, n1, n2 
    # Base Case
    if n1 == n2:
        return n1

    if root is None:
        return None
 
    # If either n1 or n2 matches with root's key, report
    #  the presence by returning root (Note that if a key is
    #  ancestor of other, then the ancestor key becomes LCA
    if root == n1 or root == n2:
        return root 
 
    # Look for keys in left and right subtrees
    left_lca = findLCAwithoutlabels(root.left, n1, n2) 
    right_lca = findLCAwithoutlabels(root.right, n1, n2)
 
    # If both of the above calls return Non-NULL, then one key
    # is present in once subtree and other is present in other,
    # So this node is the LCA
    if left_lca and right_lca:
        return root 
 
    # Otherwise check if left subtree or right subtree is LCA
    return left_lca if left_lca is not None else right_lca
 



def findLCA(root, n1, n2):
     
    # Base Case
    if root is None:
        return None
 
    # If either n1 or n2 matches with root's key, report
    #  the presence by returning root (Note that if a key is
    #  ancestor of other, then the ancestor key becomes LCA
    if root.key == n1 or root.key == n2:
        return root 
 
    # Look for keys in left and right subtrees
    left_lca = findLCA(root.left, n1, n2) 
    right_lca = findLCA(root.right, n1, n2)
 
    # If both of the above calls return Non-NULL, then one key
    # is present in once subtree and other is present in other,
    # So this node is the LCA
    if left_lca and right_lca:
        return root 
 
    # Otherwise check if left subtree or right subtree is LCA
    return left_lca if left_lca is not None else right_lca
 
 
