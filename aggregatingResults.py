"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib 
from matplotlib.pyplot import *


def avgDataSet(setname,length,TYPE):

	datapoints =[]
	file = open(TYPE+'_EXPS'+setname)
	for line in file:
	    fields = line.strip().split()

	    tmp = fields[0:length+1] #ignoring the field which has the best classification
	    tmp.pop(1)
	    datapoints.append(tmp)
	asarray =  np.array(datapoints).astype(np.float)
	avgdatapoints = np.mean(asarray, axis=0)
	maxdatapoints = asarray.max(axis=0)
	mindatapoints = asarray.min(axis=0)
	stddatapoints=  np.std(asarray, axis=0)

#	print datapoints
	return datapoints[0],maxdatapoints,mindatapoints, stddatapoints





def generatePlot(TYPE,datasets,algorithms,ylabel,thetitle,filename):


	for idx,d in enumerate(datasets):
		avgrow,maxrow,minrow,stdrow = avgDataSet(str(d), len(algorithms),TYPE)
		if TYPE == 'cost':
			divideby=  float(avgrow[0])
		else: divideby = 1	
		normalizedavgrow = [1.0*float(x)/divideby for x in avgrow]
		normalizedmaxrow = [1.0*float(x)/divideby  for x in maxrow]
		normalizedminrow = [1.0*float(x)/divideby  for x in minrow]
		normalizedstdrow = [1.0*float(x)/divideby  for x in stdrow]

		# print avgrow
		if idx==0:
			arr = np.array(normalizedavgrow)
			arrmax = np.array(normalizedmaxrow)
			arrmin = np.array(normalizedminrow)
			arrstd = np.array(normalizedstdrow)

		else:
			arr=np.vstack([arr,normalizedavgrow])	
			arrmax=np.vstack([arrmax,normalizedmaxrow])	
			arrmin=np.vstack([arrmin,normalizedminrow])	
			arrstd=np.vstack([arrstd,normalizedstdrow])	

		
	n_groups = len(datasets)
	print(n_groups)
	fig, ax = plt.subplots()
	colors = ['k', 'm','b','g','m','b','g','r','r','r','r','r']

	index = np.arange(n_groups)
	bar_width=1.0/n_groups
	
	barscale= 1.0
	if filename == 'RAND':
		scalefactor = 2.0

	elif filename == 'RAND3':
		scalefactor = 1#0.66#0.1/(n_groups)
		barscale = 0.66
	else:
		scalefactor =1.33
	error_config = {'ecolor': '0.3'}

	rects =[]
	for alg in xrange(len(algorithms)):
		# for idx, d in enumerate(datasets):
		# print arr
		opacity = 0.4
		if alg in xrange(0,4):
			opacity = 0.7

		if len(np.shape(arr)) ==1 :
			arr=[arr]
			arrstd = [arrstd]
		# print arr
		means = [row[alg] for row in arr]
		std = [row[alg] for row in arrstd]
		rects1 = plt.bar(scalefactor*index+ alg* bar_width, means, barscale*bar_width, 
	                 alpha=opacity,
	                 color=colors[alg],
	                 yerr=std,         
	                 error_kw=error_config,
	                 label=algorithms[alg])
		rects.append(rects1)
	axes = plt.gca()

	if TYPE == 'cost':
		axes.set_ylim([0.95,1.5])
		if filename == 'RAND3':
			axes.set_ylim([0.97,1.18])
		elif filename =='RAND':
			axes.set_ylim([0.97,1.7])
	else: axes.set_ylim([0,1])	


	plt.xlabel('Dataset',fontsize=18)
	plt.ylabel(ylabel,fontsize=18)
	plt.title("  ")
	plt.figtext(.5,.96,thetitle, fontsize=20, ha='center')

	prettynames = []
	for name in datasets:
		if name[0] != '_':
			prettynames.append(name)
		elif name == "NEWSGROUP":
			prettynames.append("NEWSGR.")
		elif name != "_3":
			prettynames.append(name[1:])
		else: 
			plt.xlabel("Random Graph with three clusters",fontsize=18)
			#prettynames.append("              Random Graph with three clusters")		
		print name



	plt.xticks(scalefactor*index + 1.0*bar_width  , prettynames)
	box = ax.get_position()
	ax = plt.subplot(111)


	# Put this back to print labels
	# ax.legend(loc='center left', bbox_to_anchor=(1, 0.5),title="Legend")
	# plt.gcf().subplots_adjust(bottom=0.15)

	plt.savefig(TYPE+filename+'.png',bbox_inches="tight")



filename=""
TYPES = ['cost', 'err']
datasets = ["SPLICE", "DIGITS", "NEWSGROUP",
            "BOSTON", "CANCER", "DIABETES"]
            #,"KDD"]
algorithms = ["Linkage++", "Spectral Single Linkage", "Spectral Avg. Linkage","Spectral Complete Linkage", "Graph Single Linkage", "Graph Avg. Linkage","Graph Complete Linkage"]
generatePlot(TYPES[0],datasets,algorithms, 'Normalized cost' , 'Normalized cost by algorithm' ,filename)	
generatePlot(TYPES[1],datasets,algorithms,  'Classification error', 'Classification error by algorithm',filename)	


sizes = [  512, 1024,2048]
nbclusts = [ 8,16]
datasets = []
filename="RAND"
for s in sizes:
    for c in nbclusts:
        datasets.append("_"+str(s)+"_"+str(c))

generatePlot(TYPES[0],datasets,algorithms, 'Normalized cost' , 'Normalized cost by algorithm',filename )	
generatePlot(TYPES[1],datasets,algorithms,  'Classification error', 'Classification error by algorithm',filename)	


datasets = []
filename="RAND3"
datasets.append("_3")

generatePlot(TYPES[0],datasets,algorithms, 'Normalized cost' , 'Normalized cost by algorithm',filename )	
generatePlot(TYPES[1],datasets,algorithms,  'Classification error', 'Classification error by algorithm',filename)	
 


