#!/usr/bin/python
# -*- coding: utf-8 -*-

import itertools
import matplotlib.pyplot as plt
import numpy as np
import networkx as nx
import sys
import datetime
import time
import math

from scipy.cluster.hierarchy import linkage
from sklearn import datasets
from sklearn import utils
from sklearn.decomposition import PCA
from sklearn.metrics.pairwise import cosine_similarity
from scipy.cluster import hierarchy
from sklearn.preprocessing import scale


__author__ = "Software HSBM"
__copyright__ = "Copyright (C) 2017 Vincent Cohen-Addad, Varun Kanade, and Frederik Mallmann-Trenn"
__license__ = "Public Domain"
__version__ = "9.0"
PRINTDENDRO = False
ALGORITHM ="HSBM Recovery"
class Logger(object):
    def __init__(self, filename="Default.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)
    def flush(self):
        pass

####
# Function takes as input a set of points in a
# Euclidean space and output a cosine-similarity
# weighted graph.
####
def Euclid2GraphCOS(X):
    print("nb vertices", len(X))
    G = nx.Graph()
    similarities = cosine_similarity(X, X)
    max_sim = max(max(similarities, key=lambda i: max(i)))
    min_sim = min(min(similarities, key=lambda i: min(i)))


    for i in range(len(X)):
        for j in range(i+1, len(X)):

            G.add_edge(i, j, weight=(similarities[i, j]-min_sim)/(max_sim-min_sim))
    minedgeweight = max([e for e in G.edges()], key=lambda x : G.edges[x[0], x[1]]['weight'])
    return G


####
# Function prints a dendogram
####
def print_dendro(the_array,Y,name):
    print("   DENDRO: printing a matrix shaped: ", the_array.shape)

    rgb_list = [ #red, green,     blue, yellow,      pink, turkquise,      black, brown
        "FF0000", "00FF00", "0000FF", "FFFF00", "FF00FF", "00FFFF", "000000",
        "800000", "008000", "000080", "808000", "800080", "008080", "808080",
        "C00000", "00C000", "0000C0", "C0C000", "C000C0", "00C0C0", "C0C0C0",
        "400000", "004000", "000040", "404000", "400040", "004040", "404040",
        "200000", "002000", "000020", "202000", "200020", "002020", "202020",
        "600000", "006000", "000060", "606000", "600060", "006060", "606060",
        "A00000", "00A000", "0000A0", "A0A000", "A000A0", "00A0A0", "A0A0A0",
        "E00000", "00E000", "0000E0", "E0E000", "E000E0", "00E0E0", "E0E0E0",
    ]
    the_length = the_array.shape[0]+1

    plt.clf()

    rgb = {i : "#"+rgb_list[i] for i in range(len(rgb_list))}

    hierarchy.dendrogram(the_array[:,:] ,color_threshold=0.00001,labels=[i for i in range(the_length)])

    label_colors = {i: rgb[Y[i]] for i in range(the_length)}

    ax = plt.gca()
    xlbls = ax.get_xmajorticklabels()
    for lbl in xlbls:
        if len(lbl.get_text()) > 0:
            lbl.set_color(label_colors[int(lbl.get_text())])
    plt.savefig(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')+name+'_plot.png')


def cut(G, S1, S2):
    c = 0.0
    for v in S1:
        for u in S2:
            if G.has_edge(u,v) and 'weight' in G.edges[u, v]:
                c = c + G.edges[u, v]['weight']
            elif G.has_edge(u,v):
                c = c+1.0
    return c




def normalizehalf(dendro):
    maxval = max(dendro, key=lambda a : a[2])[2]
    minval = min(dendro, key=lambda a : a[2])[2]
    for i in range(len(dendro)):
        dendro[i][2] = (dendro[i][2]-0.99*minval)/(2*(maxval-0.99*minval))

####
# Count the # of differences between equal length strings str1 and str2
####
def hamdist(str1, str2):
    diffs = 0
    for ch1, ch2 in zip(str1, str2):
            if ch1 != ch2:
                    diffs += 1
    return diffs


####
# Compute error between cluster and target cluster
# param cluster: proposed cluster
# param target_cluster: target cluster
# return: error
####
def errorPermutation(G0, dendro, target_cluster):
    from munkres import Munkres
    clust = list(set(target_cluster))
    k = len(clust)
    n = len(target_cluster)

    cluster_tmp = GetClustersFromDendro(G0, dendro, len(dendro)-k+1)

    if n != len(G0.nodes()) or n != len(cluster_tmp):
        raise Exception("Weird stuff is happening somehow")
    cluster = []
    setids = list(set(cluster_tmp))


    for i in range(n):
        cluster.append(setids.index(cluster_tmp[i]))

    nicetargets = target_cluster[:]
    for i in range(len(list(set(target_cluster)))):
        nicetargets=[i if float(x)==float(list(set(target_cluster))[i]) else x for x in nicetargets]


    mindist = n
    setofpermutation =  set(cluster) if len(cluster) >= len(set(target_cluster)) else set(nicetargets)
    for iteration in itertools.permutations(setofpermutation):
        permutedstring = []
        for item in cluster:
            permutedstring.append(list(iteration).index(item))
        mindist = min(mindist, hamdist(list(target_cluster),permutedstring))
    return float(mindist)/float(n)

####
# This code belongs to Aurko Roy and
# Sebastian Pokutta.
# Compute error between cluster and target cluster
# param cluster: proposed cluster
# param target_cluster: target cluster
# return: error
####
def error(G0, dendro, target_cluster):
    
    from munkres import Munkres
    clust = list(set(target_cluster))
    k = len(clust)
    n = len(target_cluster)

    cluster_tmp = GetClustersFromDendro(G0, dendro, len(dendro)-k+1)

    if n != len(G0.nodes()) or n != len(cluster_tmp):
        raise Exception("Weird stuff is happening somehow")
    cluster = []
    setids = list(set(cluster_tmp))


    for i in range(n):
        cluster.append(setids.index(cluster_tmp[i]))


    C = []
    T = []
    for i in clust:
        tmp = {j for j in range(n) if cluster[j] == i}
        C.append(tmp)
        tmp = {j for j in range(n) if target_cluster[j] == i}
        T.append(tmp)
    M = []
    for i in range(k):
        M.append([0]*k)
    testM = []
    for i in range(k):
        testM.append([0]*k)
    for i in range(k):
        for j in range(k):
            M[i][j] = len(C[i].difference(T[j]))
            testM[i][j] = len(T[j].difference(C[i]))
    m = Munkres()
    indexes = m.compute(M)
    total = 0
    for row, col in indexes:
        value = M[row][col]
        total += value
    indexes2 = m.compute(testM)
    total2 = 0
    for row, col in indexes2:
        value = testM[row][col]
        total2 += value
    err =  float(total)/float(n)
    return err


####
# Compute the cost of a dendrogram w.r.t. to the DasGupta function
####
def cost(dendro, G0):
    cluster_cuts = {}
    NUMBEROFELEMENTS = G0.number_of_nodes()
    for i in range(NUMBEROFELEMENTS):
        cluster_cuts[i]= {}

    for i, j in list(itertools.combinations(range(NUMBEROFELEMENTS), r=2)):
        cutij = cut(G0, [j], [i])
        cluster_cuts[i][j]= cluster_cuts[j][i] = cutij

    cost = 0
    current_clusters = [i for i in range(NUMBEROFELEMENTS)]
    for step in range(len(dendro)):
        new_clust_id = step + NUMBEROFELEMENTS
        costNODE = cluster_cuts[dendro[step][0]][dendro[step][1]] * dendro[step][3]
        current_clusters.remove(dendro[step][0])
        current_clusters.remove(dendro[step][1])
        cluster_cuts[new_clust_id] = {}
        for j in current_clusters:
            cluster_cuts[j][new_clust_id] = cluster_cuts[new_clust_id][j] = cluster_cuts[dendro[step][0]][j] + cluster_cuts[dendro[step][1]][j]
        current_clusters.append(new_clust_id)
        cost+=costNODE
    return cost


####
# First uses PCA. The remaining levels of the hierarchy are computed by using the linkage method given as a parameter
####
def run_PHASE1(G, COMPO,use_method='single'):
    NUMBEROFELEMENTS = len(G.nodes())
    adjaceny_matrix = nx.to_numpy_matrix(G)
    X_reduced = PCA(n_components=COMPO, 
                    ).fit_transform(adjaceny_matrix)

    dendro = linkage(X_reduced, method=use_method, metric='euclidean')
    normalizehalf(dendro)

    return dendro, X_reduced

def GetClustersFromDendro(G, dendro, breakpoint):
    psi = [i for i in range(G.number_of_nodes())]
    for i in range(breakpoint):
        for j in range(G.number_of_nodes()):
            if psi[j] == dendro[i][0] or psi[j] == dendro[i][1]:
                psi[j] = G.number_of_nodes() + i
    return psi






####
## Args are in current notation: G, ids, psi
## Output: dendrogram
####
def DensityBasedLinkage(G, cluster_ids, vertex_to_ids):

    import heapq as hq

    h = []
    NUMBEROFELEMENTS = G.number_of_nodes() 
    time_steps = 0
    after_merge = []
    cluster_cuts = {i: {} for i in cluster_ids}
    cluster_distance = {i: {} for i in cluster_ids}

    cluster_elements = {i: [] for i in cluster_ids}
    cluster_sizes = {}

    for x in range(len(vertex_to_ids)):
        cluster_elements[vertex_to_ids[x]].append(x)

    
    for i in cluster_elements:
        cluster_sizes[i] = len(cluster_elements[i])

    
    nbelems = sum([cluster_sizes[i] for i in cluster_sizes])

    for i, j in list(itertools.combinations(cluster_ids, r=2)):
        cutij = cut(G, cluster_elements[j], cluster_elements[i])
        dist = float(cutij)/float(cluster_sizes[j]*cluster_sizes[i])
        h.append((1.0-(dist), i, j))
        cluster_distance[i][j]= cluster_distance[j][i] = dist
        cluster_cuts[i][j]= cluster_cuts[j][i] = cutij

    h.sort()

    current_clusters = [i for i in cluster_ids]

    remaining_steps = 2*nbelems-1 - NUMBEROFELEMENTS - (NUMBEROFELEMENTS-len(current_clusters)-1)
    nbmerges = 2*nbelems-1 - remaining_steps+1 

    safe = len(current_clusters)

    dendrogram = [[] for i in range(2*nbelems)]
    nbmerges=2*nbelems-1-safe+1

    nbiter = nbmerges

    print("   Number of iterations to be done", 2*nbelems-1 - nbmerges )
    while nbmerges < 2*nbelems-1:
        if nbiter % 100 == 0:
            print("   Iter", nbiter, "over", 2*nbelems-1)
        nbiter+=1
        densestcut = 0.0
        imax = 0
        jmax = 0

        (densestcut, imax, jmax) = hq.heappop(h)
        while (imax not in current_clusters) or (jmax not in current_clusters):
            (densestcut, imax, jmax) = hq.heappop(h)

        dendrogram[nbmerges] = [imax, jmax, densestcut, cluster_sizes[imax]+cluster_sizes[jmax]]

        if nbmerges <= imax or nbmerges <= jmax:
            raise(Exception("weirrrd"))
        new_clust_id = nbmerges
        cluster_sizes[new_clust_id] = dendrogram[nbmerges][3]
        current_clusters.remove(imax)
        current_clusters.remove(jmax)
        cluster_cuts[new_clust_id] = {}
        cluster_distance[new_clust_id] = {}

        for j in current_clusters:
            cluster_cuts[j][new_clust_id] = cluster_cuts[new_clust_id][j] = cluster_cuts[imax][j] + cluster_cuts[jmax][j]

            newvalue = float(max(cluster_distance[imax][j], cluster_distance[jmax][j]))
            cluster_distance[j][new_clust_id] = cluster_distance[new_clust_id][j] = newvalue

            
            hq.heappush(h, (1.0-(newvalue),new_clust_id, j))

        current_clusters.append(new_clust_id)
        nbmerges = nbmerges+1
    return dendrogram 




def DensityBasedLinkageOLD(G, cluster_ids, vertex_to_ids):

    import heapq as hq
    print("USING OLD LINKAGE")
    h = []
    NUMBEROFELEMENTS = G.number_of_nodes() 
    time_steps = 0
    after_merge = []
    cluster_cuts = {i: {} for i in cluster_ids}
    cluster_elements = {i: [] for i in cluster_ids}
    cluster_sizes = {}

    for x in range(len(vertex_to_ids)):
        cluster_elements[vertex_to_ids[x]].append(x)

    # print("done elements")
    for i in cluster_elements:
        cluster_sizes[i] = len(cluster_elements[i])

    # print("done sizes")
    nbelems = sum([cluster_sizes[i] for i in cluster_sizes])

    for i, j in list(itertools.combinations(cluster_ids, r=2)):
        cutij = cut(G, cluster_elements[j], cluster_elements[i])
        h.append((1-(float(cutij)/float(cluster_sizes[j]*cluster_sizes[i])), i, j))
        cluster_cuts[i][j]= cluster_cuts[j][i] = cutij

    h.sort()

    current_clusters = [i for i in cluster_ids]
    
    remaining_steps = 2*nbelems-1 - NUMBEROFELEMENTS - (NUMBEROFELEMENTS-len(current_clusters)-1)
    nbmerges = 2*nbelems-1 - remaining_steps+1 #2*nbelems-1 - NUMBEROFELEMENTS - breakpoint # which is basically nb_elems + nb_merges made on first phase

    safe = len(current_clusters)

    dendrogram = [[] for i in range(2*nbelems)]
    nbmerges=2*nbelems-1-safe+1
   
    nbiter = nbmerges

    print("   Number of iterations to be done", 2*nbelems-1 - nbmerges )
    while nbmerges < 2*nbelems-1:
        if nbiter % 100 == 0:
            print("   Iter", nbiter, "over", 2*nbelems-1)
        nbiter+=1
        densestcut = 0.0
        imax = 0
        jmax = 0

        (densestcut, imax, jmax) = hq.heappop(h)
        while (imax not in current_clusters) or (jmax not in current_clusters):
            (densestcut, imax, jmax) = hq.heappop(h)

        dendrogram[nbmerges] = [imax, jmax, densestcut, cluster_sizes[imax]+cluster_sizes[jmax]]
        
        if nbmerges <= imax or nbmerges <= jmax:
            raise(Exception("weirrrd"))
        new_clust_id = nbmerges
        cluster_sizes[new_clust_id] = dendrogram[nbmerges][3]
        current_clusters.remove(imax)
        current_clusters.remove(jmax)
        cluster_cuts[new_clust_id] = {}
        for j in current_clusters:
            cluster_cuts[j][new_clust_id] = cluster_cuts[new_clust_id][j] = cluster_cuts[imax][j] + cluster_cuts[jmax][j]
            hq.heappush(h, (1-(float(cluster_cuts[imax][j] + cluster_cuts[jmax][j])/float(cluster_sizes[new_clust_id]*cluster_sizes[j])),
                            new_clust_id, j))

        current_clusters.append(new_clust_id)
        nbmerges = nbmerges+1
    return dendrogram 







def ClassicDensity(G, f = min):
    import heapq as hq
    h = []

    NUMBEROFELEMENTS = G.number_of_nodes()
    time_steps = 0
    after_merge = []
    cluster_ids = [i for i in range(NUMBEROFELEMENTS)]
    cluster_cuts = {i: {} for i in cluster_ids}
    cluster_elements = {i: [i] for i in cluster_ids}
    cluster_sizes = {i : 1 for i in cluster_ids}

    for i, j in list(itertools.combinations(cluster_ids, r=2)):
        if G.has_edge(i,j) and 'weight' in G.edges[i, j]:
            dij = G.edges[i, j]['weight']
        elif G.has_edge(i,j):
            dij = 1.0
        else :
            dij = 0.0
        h.append((1-dij, i, j))
        cluster_cuts[i][j]= cluster_cuts[j][i] = dij

    h.sort()

    current_clusters = [i for i in cluster_ids]
    nbelems = NUMBEROFELEMENTS
    remaining_steps = 2*nbelems-1 - NUMBEROFELEMENTS - (NUMBEROFELEMENTS-len(current_clusters)-1)
    nbmerges = 2*nbelems-1 - remaining_steps+1
    safe = len(current_clusters)
    dendrogram = [[] for i in range(2*nbelems)]
    nbmerges=2*nbelems-1-safe+1
    nbiter = nbmerges

    print("   Number of iterations to be done", 2*nbelems-1 - nbmerges )
    while nbmerges < 2*nbelems-1:
        if nbiter % 100 == 0:
            print("   Itereration", nbiter, "over", 2*nbelems-1)
        nbiter+=1
        densestcut = 0.0
        imax = 0
        jmax = 0


        (densestcut, imax, jmax) = hq.heappop(h)
        while (imax not in current_clusters) or (jmax not in current_clusters):
            (densestcut, imax, jmax) = hq.heappop(h)

        dendrogram[nbmerges] = [imax, jmax, densestcut, cluster_sizes[imax]+cluster_sizes[jmax]]
        if nbmerges <= imax or nbmerges <= jmax:
            raise(Exception("weirrrd"))
        new_clust_id = nbmerges
        cluster_sizes[new_clust_id] = dendrogram[nbmerges][3]
        current_clusters.remove(imax)
        current_clusters.remove(jmax)
        cluster_cuts[new_clust_id] = {}
        for j in current_clusters:
            cluster_cuts[j][new_clust_id] = cluster_cuts[new_clust_id][j] = f(cluster_cuts[imax][j],cluster_cuts[jmax][j])
            hq.heappush(h, (1-f(cluster_cuts[imax][j],cluster_cuts[jmax][j]), new_clust_id, j))
        current_clusters.append(new_clust_id)
        nbmerges = nbmerges+1
    return dendrogram[2*nbelems-1-safe+1: nbmerges] 




def output(filename, results):
    f_err = open("err_"+filename, "a+")
    f_cost = open("cost_"+filename, "a+")

    for cost,err in results:
        f_err.write(str(err)+" ")
        f_cost.write(str(cost)+" ")

    f_err.write("\n")
    f_cost.write("\n")
    f_err.close()
    f_cost.close()





######################################################################
######################################################################
#STARTING MAIN
#Output: 1) println of Dasgupta cost and classiciation error using different clustering approaches:
#        2) file consists of tuples (cost of X, Classification Error X),
#           where the first tuple of the file is our best cost (depending on the breakpoint)
#           the ohter tuples are,  singlelinkage, density, compl and single, followed by other tuples corresponding
#           to different split points of our algoirhtm
######################################################################
######################################################################
def main(USE, filename, sizesynthetic = 256, nbclustersynthetic = 4):
    sys.stdout = Logger(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S') + ".txt")
    if USE=='IRIS':
        our_dataset = datasets.load_iris()
        data = our_dataset.data[:, :2]
    elif USE=='DIGITS':
        from sklearn.datasets import load_digits
        our_dataset = load_digits()
        data = scale(our_dataset.data)
    elif USE=='NEWSGROUP':
        from sklearn.datasets import fetch_20newsgroups
        from sklearn.feature_extraction.text import TfidfVectorizer
        cats = ['comp.graphics',
                'comp.os.ms-windows.misc',
                'comp.sys.ibm.pc.hardware',
                'comp.sys.mac.hardware',
                # 'comp.windows.x',
                # 'rec.autos',
                # 'rec.motorcycles',
                'rec.sport.baseball',
                'rec.sport.hockey']
        print(cats)
        our_dataset = fetch_20newsgroups(subset='train',
                                         categories=cats)
        vectorizer = TfidfVectorizer()
        data_sparse = vectorizer.fit_transform(our_dataset.data)
        data= data_sparse.todense()
        # exit()
    elif USE=='BOSTON':
        from sklearn.datasets import load_boston
        our_dataset = load_boston()
        data = scale(our_dataset.data)
    elif USE=='CANCER':
        from sklearn.datasets import load_breast_cancer
        our_dataset = load_breast_cancer()
        data = scale(our_dataset.data)
    elif USE=='DIABETES':
        from sklearn.datasets import load_diabetes
        our_dataset = load_diabetes()
        data = scale(our_dataset.data)
    elif USE=='KDD':
        from sklearn.datasets import fetch_kddcup99
        our_dataset = fetch_kddcup99(subset='SF')
        data = scale(our_dataset.data[:2000, [0,2,3]])
    elif USE=='SYNTHETIC':
        from randomgraph import binarylimits, binarylimitsspecial
        if nbclustersynthetic!= 3:
            G, labels, groundtruth = binarylimits(sizesynthetic,
                                              nbclustersynthetic)
        else:
            print("Generating special tree")
            G, labels, groundtruth = binarylimitsspecial(sizesynthetic, nbclustersynthetic)

        print("Graph generation completed.")



    if USE != 'SYNTHETIC':
        print("Starting graph generation")
        G = Euclid2GraphCOS(data)
        labels = our_dataset.target[:len(G.nodes())]
        for i in range(len(labels)):
            print(labels[i])
            print(data)

    print("Graph Generated")
    NUMBEROFELEMENTS = len(G.nodes())
    print("dataset:", USE, "nodes:", NUMBEROFELEMENTS,"Ground-truth number of classes:", len(set(labels)))

    psi = [i for i in range(NUMBEROFELEMENTS)]
    cluster_sizes = [i for i in range(1,  NUMBEROFELEMENTS+1)]
    test_only_correctleavesize = True
    COMPO = len(set(labels))

    to_merge, X_reduced = run_PHASE1(G, COMPO)
    PCA_singlelinkage_cost = cost(to_merge, G)
    PCA_singlelinkage_err = error(G, to_merge, labels)

    to_merge, X_reduced = run_PHASE1(G, COMPO,use_method='average')
    PCA_averagelinkage_cost = cost(to_merge, G)
    PCA_averagelinkage_err = error(G, to_merge, labels)

    to_merge, X_reduced = run_PHASE1(G, COMPO,use_method='complete')
    PCA_completelinkage_cost = cost(to_merge, G)
    PCA_completelinkage_err = error(G, to_merge, labels)

    complete = ClassicDensity(G, f=max)
    complete_cost = cost(complete, G)
    complete_err = error(G, complete, labels)

    single = ClassicDensity(G, f=min)
    single_cost = cost(single, G)
    single_err = error(G, single, labels)


    ##### DensityBasedLinkage from bottom to top
    psi = GetClustersFromDendro(G, to_merge, 0)
    ids = sorted(list(set(psi)))
    print("   Starting DensityBasedLinkage", " ")
    d = DensityBasedLinkage(G, [i for i in range(NUMBEROFELEMENTS)],
                            [i for i in range(NUMBEROFELEMENTS)])
    densitybased_dendro = d[NUMBEROFELEMENTS:NUMBEROFELEMENTS-1+NUMBEROFELEMENTS]
    densitybased_cost = cost(densitybased_dendro, G)
    densitybased_err = error(G, densitybased_dendro, labels)

    #####Generate values to be tested
    break_values = []
    C =len(set(labels))
    break_values = [int(0.3*(NUMBEROFELEMENTS)),int(0.5*(NUMBEROFELEMENTS)),int(0.85*(NUMBEROFELEMENTS)),int(0.9*(NUMBEROFELEMENTS)),int(0.95*(NUMBEROFELEMENTS)),int(0.97*(NUMBEROFELEMENTS))]
    break_values2 =[NUMBEROFELEMENTS-2*COMPO-1,NUMBEROFELEMENTS-COMPO-1, NUMBEROFELEMENTS-COMPO]
    break_values = break_values+break_values2
    break_values.append(NUMBEROFELEMENTS-1)
    break_values.append(0)
    break_values = [item for item in break_values if item >= 0]
    print ("Testing these break values:", sorted(list(set(break_values))) )

    result =[]
    break_values.sort()


    for idx,i in enumerate(break_values):
        print("Total Progress", idx, "of", len(break_values), "Percentage: ", 1.0*idx/len(break_values))
        result.append(run(G, to_merge, i, labels))


    print("#######################################")
    print("##############", USE, "#########################")
    print("COST DensityBasedLinkage", densitybased_cost, densitybased_err)
    print("COST SingleLinkage", single_cost, single_err)
    print("COST CompleteLinkage", complete_cost, complete_err)
    print("COST SignleLinkage in PCA space", PCA_singlelinkage_cost, PCA_singlelinkage_err)
    print("COST AverageLinkage in PCA space", PCA_averagelinkage_cost, PCA_averagelinkage_err)
    print("COST CompleteLinkage in PCA space", PCA_completelinkage_cost, PCA_completelinkage_err)
    bestthem = min([densitybased_cost, PCA_singlelinkage_cost, single_cost, complete_cost,PCA_averagelinkage_cost,PCA_completelinkage_cost])
    minres = max(result, key = lambda x : x[0])
    for i in range(0,len(result)-2):
        print("For breakpoint", break_values[i+1], "cost and err are", result[i+1])
        if minres[0] >= result[i+1][0]:
            valres = i+1
            minres = result[i+1]
    bestus = result[valres][0]
    ourresults = result[1:len(result)-2]
    #The first value contains
    to_print_us = [min(ourresults, key=lambda x: x[0]), min(ourresults, key=lambda x: x[1])]

    pairs = [(PCA_singlelinkage_cost, PCA_singlelinkage_err),
    (PCA_averagelinkage_cost, PCA_averagelinkage_err),(PCA_completelinkage_cost, PCA_completelinkage_err),
             (single_cost, single_err),(densitybased_cost, densitybased_err),(complete_cost, complete_err)]
    if filename != -1:
        
        output(filename, to_print_us+pairs+result)
    print("BEST: for breakpoint", break_values[valres], "of cost diff", bestus - bestthem, "(<0 good)")
    print("DensityBasedLinkage - bestus", densitybased_cost - bestus , " (>0 good)")
    if densitybased_cost != 0:
        print("bestus/densitybased_cost", 1.0*bestus/densitybased_cost, " (<1 good)")


    print("SingleLinkage in PCA space - bestus", PCA_singlelinkage_cost - bestus, " (>0 good)")
    if PCA_singlelinkage_cost != 0:
        print("bestus/SingleinPCA_cost", 1.0*bestus/PCA_singlelinkage_cost, " (<1 good)")
    print("Average in PCA space - bestus", PCA_averagelinkage_cost - bestus, " (>0 good)")
    if PCA_singlelinkage_cost != 0:
        print("bestus/SingleinPCA_cost", 1.0*bestus/PCA_averagelinkage_cost, " (<1 good)")
    print("CompleteLinkage in PCA space - bestus", PCA_completelinkage_cost - bestus, " (>0 good)")
    if PCA_completelinkage_cost != 0:
        print("bestus/SingleinPCA_cost", 1.0*bestus/PCA_completelinkage_cost, " (<1 good)")

    if densitybased_cost - bestus >0:
        print("DBL: " +ALGORITHM+" WINS")
    elif densitybased_cost - bestus== 0 :
        print("DBL: TIE")
    else:
        print("DBL: FAIL")

    if single_cost - bestus >0:
        print("SI: " +ALGORITHM+" WINS")
    elif single_cost - bestus== 0 :
        print("SI: TIE")
    else:
        print("SI: FAIL")

    if complete_cost - bestus >0:
        print("CO: " +ALGORITHM+" WINS")
    elif complete_cost - bestus== 0 :
        print("CO: TIE")
    else:
        print("CO: FAIL")

    if PCA_singlelinkage_cost - bestus >0:
        print("SingleInPCA: " +ALGORITHM+" WINS")
    elif PCA_singlelinkage_cost - bestus== 0 :
        print("SingleInPCA: TIE")
    else:
        print("SingleInPCA: FAIL")

    if PCA_averagelinkage_cost - bestus >0:
        print("AverageInPCA: " +ALGORITHM+" WINS")
    elif PCA_averagelinkage_cost - bestus== 0 :
        print("AverageInPCA: TIE")
    else:
        print("AverageInPCA: FAIL")

    if PCA_completelinkage_cost - bestus >0:
        print("CompleteInPCA: " +ALGORITHM+" WINS")
    elif PCA_completelinkage_cost - bestus== 0 :
        print("CompleteInPCA: TIE")
    else:
        print("CompleteInPCA: FAIL")

    if PRINTDENDRO:
        print_dendro(to_merge,labels,"Dendro_PCA+Single")



def run(G, to_merge, breakpoint, labels):
    #breakpoing first point where we use density based linkage
    NUMBEROFELEMENTS = len(G.nodes())


    print("Break_nbr_merges", breakpoint)

    psi = GetClustersFromDendro(G, to_merge, breakpoint) #This includes the point breakpoint

    ids = sorted(list(set(psi)))

    print("   Starting DensityBasedLinkage", " ")
    d = DensityBasedLinkage(G, ids, psi)

    if breakpoint < NUMBEROFELEMENTS-1:
        maxd = max(d[NUMBEROFELEMENTS+breakpoint:NUMBEROFELEMENTS-1+NUMBEROFELEMENTS], key=lambda a : a[2])[2]
        mind = min(d[NUMBEROFELEMENTS+breakpoint:NUMBEROFELEMENTS-1+NUMBEROFELEMENTS], key=lambda a : a[2])[2]



    #MERGING THE ARRAYS
    merged = []
    for i in range(NUMBEROFELEMENTS-1):
        if i < breakpoint:
            merged.append([to_merge[i][0],to_merge[i][1],to_merge[i][2],to_merge[i][3]])
        else:
            if maxd == 0 and mind == 0:
                v = 0.5
            else:
                v = 0.5+ ((d[NUMBEROFELEMENTS+i][2]-0.99*mind)/(maxd-0.99*mind))
            merged.append([d[NUMBEROFELEMENTS+i][0],
                           d[NUMBEROFELEMENTS+i][1],
                           v,
                           d[NUMBEROFELEMENTS+i][3]])

    merged = np.array(merged)


    merged_cost = cost(merged, G)


    if breakpoint != 0 and PRINTDENDRO:
        print_dendro(merged,labels,"DENDRO_US_"+str(breakpoint))
    print("MERGED COSTS WERE", merged_cost, "for breakpoint", breakpoint)
    print("######################################################")
    print("######################################################")
    return (merged_cost, error(G, merged, labels))


    ####################################################
    #### DEBUG BELOW ###################################
    #### check if we merge a cluster that does not exist
    ####################################################
    DEBUG = False
    if DEBUG:
        print("Running DEBUG")
        for i in range(len(merged)):
            print(merged[i])
            if NUMBEROFELEMENTS+i <= merged[i][0] or NUMBEROFELEMENTS+i <= merged[i][1]:
                print("we are merging a non-existing cluster", NUMBEROFELEMENTS+i, merged[i][0], merged[i][1])

        print("----------")

        ### DEBUG: check if we merge a cluster multiple times
        seen = []
        for i in range(len(merged)):
            u = merged[i][0]
            v = merged[i][1]
            if u in seen or v in seen:
                print("Already seen ", i, u in seen, v in seen, merged[i])
            seen.append(u)
            seen.append(v)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        USE = 'IRIS'
    else:
        USE = sys.argv[1]

    if len(sys.argv) < 3:
        filename = -1
        main(USE, filename)
    else:
        filename = sys.argv[2]
        print(filename)
        if len(sys.argv) > 3:
            sizesynthetic = int(sys.argv[3])
            nbclustersynthetic = int(sys.argv[4])
            main(USE, filename, sizesynthetic = sizesynthetic, nbclustersynthetic = nbclustersynthetic)
        else:
            main(USE, filename)


