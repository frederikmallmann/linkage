import networkx as nx
import itertools
from main import print_dendro
from Tree import *
import numpy as np



def GetGTTree(bigleavessizes, proba, labels):
    from scipy.cluster.hierarchy import linkage
    from copy import deepcopy
    n = sum(bigleavessizes)
    distancematrix = []
    for u in range(n):
        proba_u = deepcopy(proba[labels[u]])
        all_proba_u = [] 
        for j in range(len(proba_u)):
            all_proba_u += [1-proba_u[j]]*bigleavessizes[j]
        all_proba_u[u] = 0
        distancematrix.append(deepcopy(all_proba_u))

    dendro = linkage(distancematrix, method='single')
    return dendro    
    
def GenerateGraph(proba, bigleavessizes):

    n = sum(bigleavessizes)
    G = nx.Graph()
    labels = []
    i = 0
    for lab in range(len(bigleavessizes)):
        for u in range(bigleavessizes[lab]):
            labels.append(lab)
            i+=1
    count = 0

    permuted = np.random.permutation(n)
    cpylabels = labels[:]
    labels = [cpylabels[permuted[x]-1] for x in range(n)  ]


    for u,v in list(itertools.combinations(range(n), r=2)):
        clustu = labels[u]
        clustv = labels[v]
        coin = np.random.random() 
        if coin < proba[clustu][clustv]:
            count = count + 1
            G.add_edge(u,v, weight=1.0)
        else:        
            G.add_edge(u,v, weight=0.0)


            # exit()
    print "Number of edges:", count      
    return G, labels

def generateTree(layers, prob,  bonus):
    if layers == 0:
        return Node(0, min(prob,1),0)
    newNode = Node(0, min(prob,1))
    left = generateTree( layers-1, prob+bonus,bonus)
    newNode.left = left
    right= generateTree( layers-1, prob+bonus,bonus)
    newNode.right = right
    return newNode

def binarylimits(n, k):
    import math
    from copy import deepcopy

    print("PARAMS", n, k    )

    bigleavessizes = []
    for i in range(k-1):
        bigleavessizes.append(int(n/k))
    bigleavessizes.append(n-sum(bigleavessizes))    
    # bigleavessizes = [int(2*n/(k)),int(n/(2*k)),int(n/(2*k)),int(n/(2*k)),int(n/(2*k)),int(n/(k)),int(n/(k)),0  ]
    # bigleavessizes[k-1]=n-sum(bigleavessizes)
    #[int(n/k)]*k
    print (sum(bigleavessizes), len(bigleavessizes))

    logk = int(math.log(k,2))
    pmin = 2.0*math.log(n)*(k)/n #float(math.log(n))/float(n)
    layers = logk+1
    pmax = 1#min(1, pmin * layers )
    
    bonus = 1.0*(pmax-pmin)/layers

    root = generateTree(layers,pmin,bonus)

    root.setphi(0)
    #------------- GENERATE Prob Matrix

    thatlist = root.generateListofLeaves()[:k]

    pairs = list(itertools.combinations_with_replacement(thatlist, 2))

    Matrix = [[0. for i in range(k)] for elem in range(k)]
    for elem in sorted(pairs):
        LCA = findLCAwithoutlabels(root,elem[0],elem[1])
        Matrix[elem[0].phi][elem[1].phi] = LCA.prob
        Matrix[elem[1].phi][elem[0].phi] = LCA.prob

 

    
    
    
    r=0.39
    q=0.49
    p1 = 1
    p2 = 0.49
    p3= 0.62
    Matrix = np.array([[p1,q,r],[q,p2,r],[r,r,p3]]) 

    bigleavessizes =[int(n/3),int(n/3)]
    bigleavessizes.append(n-sum(bigleavessizes))  





    print("Probability Matrix:" )
    # if k < 20:
    print(np.matrix(Matrix))

    G, labels = GenerateGraph(Matrix, bigleavessizes)
    dendro = GetGTTree(bigleavessizes, Matrix, labels)
    return G, labels, dendro


