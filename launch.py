import sys
import subprocess
import main as main_module



sizes = [256, 512, 1024, 2048]
nbclusts = [ 2,4,8, 16]#,64]

datasets = ["IRIS", "DIGITS", "NEWSGROUP",  "BOSTON", "CANCER", "DIABETES"]



def main():
	for i in range(20):
		main_module.main("SYNTHETIC","EXPS_3",3000 , 3 )
		for s in sizes:
			for c in nbclusts:
				main_module.main("SYNTHETIC","EXPS_"+str(s)+"_"+str(c), s , c )
		for d in datasets: 
			main_module.main(d, "EXPS"+d)    



main()
